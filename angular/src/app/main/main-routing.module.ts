import { ViewCustomerProfileComponent } from './view-customer-profile/view-customer-profile.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProductRecommenderSystemNBOComponent } from './productRecommenderSystem/productRecommenderSystemNBO/productRecommenderSystemNBO.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CTDashboardComponent } from './ctdashboard/ctdashboard.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: 'productRecommenderSystem/productRecommenderSystemNBO', component: ProductRecommenderSystemNBOComponent, data: { permission: 'Pages.ProductRecommenderSystemNBO' }  },
                    { path: 'dashboard', component: DashboardComponent, data: { permission: 'Pages.Tenant.Dashboard' }},
                    { path: 'ctdashboard', component: CTDashboardComponent, data: { permission: 'Pages.Tenant.Dashboard' }},
                    { path: 'ctdashboard/:churnStat', component: CTDashboardComponent, data: { permission: 'Pages.Tenant.Dashboard' }},
                    { path: 'customerdashboard', component: ViewCustomerProfileComponent, data: { permission: 'Pages.Tenant.Dashboard' }}
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class MainRoutingModule { }
