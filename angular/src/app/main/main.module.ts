import { AppComponent } from './../app.component';
import { ViewCustomerProfileComponent } from './view-customer-profile/view-customer-profile.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { ProductRecommenderSystemNBOComponent } from './productRecommenderSystem/productRecommenderSystemNBO/productRecommenderSystemNBO.component';
import { ViewProductRecommenderSystemNBOModalComponent } from './productRecommenderSystem/productRecommenderSystemNBO/view-productRecommenderSystemNBO-modal.component';
import { CreateOrEditProductRecommenderSystemNBOModalComponent } from './productRecommenderSystem/productRecommenderSystemNBO/create-or-edit-productRecommenderSystemNBO-modal.component';
import { AutoCompleteModule } from 'primeng/primeng';
import { PaginatorModule } from 'primeng/primeng';
import { EditorModule } from 'primeng/primeng';
import { InputMaskModule } from 'primeng/primeng';
import { FileUploadModule } from 'primeng/primeng';
import { TableModule } from 'primeng/table';

import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { BrowserModule } from '@angular/platform-browser';
import { AgmCoreModule } from '@agm/core';
import {
    ModalModule,
    TabsModule,
    TooltipModule,
    BsDropdownModule,
    PopoverModule
} from 'ngx-bootstrap';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainRoutingModule } from './main-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import {
    BsDatepickerModule,
    BsDatepickerConfig,
    BsDaterangepickerConfig,
    BsLocaleService
} from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { CTDashboardComponent } from './ctdashboard/ctdashboard.component';

NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

@NgModule({
    imports: [
        // tslint:disable-next-line:indent
        FileUploadModule,
        AutoCompleteModule,
        PaginatorModule,
        EditorModule,
        InputMaskModule,
        TableModule,
        BrowserModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBCrReZxiniV3kWoyMmpDNvTXNRy4cJRL4'
        }),
        CommonModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        MainRoutingModule,
        CountoModule,
        NgxChartsModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        PopoverModule.forRoot()
    ],
    declarations: [
        ViewCustomerProfileComponent,
        ProductRecommenderSystemNBOComponent,
        ViewProductRecommenderSystemNBOModalComponent,
        CreateOrEditProductRecommenderSystemNBOModalComponent,
        DashboardComponent,
        CTDashboardComponent,
        AppComponent
    ],
    providers: [
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory:
                NgxBootstrapDatePickerConfigService.getDaterangepickerConfig
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale
        }
    ],
    bootstrap: [AppComponent]
})
export class MainModule {}
