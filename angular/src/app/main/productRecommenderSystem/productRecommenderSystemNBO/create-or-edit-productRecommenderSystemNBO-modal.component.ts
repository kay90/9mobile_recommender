import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { ProductRecommenderSystemNBOServiceProxy, CreateOrEditProductRecommenderSystemNBODto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';


@Component({
    selector: 'createOrEditProductRecommenderSystemNBOModal',
    templateUrl: './create-or-edit-productRecommenderSystemNBO-modal.component.html'
})
export class CreateOrEditProductRecommenderSystemNBOModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;


    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    productRecommenderSystemNBO: CreateOrEditProductRecommenderSystemNBODto = new CreateOrEditProductRecommenderSystemNBODto();

            lastTransactionDate: Date;


    constructor(
        injector: Injector,
        private _productRecommenderSystemNBOServiceProxy: ProductRecommenderSystemNBOServiceProxy
    ) {
        super(injector);
    }

    show(productRecommenderSystemNBOId?: number): void {
this.lastTransactionDate = null;

        if (!productRecommenderSystemNBOId) {
            this.productRecommenderSystemNBO = new CreateOrEditProductRecommenderSystemNBODto();
            this.productRecommenderSystemNBO.id = productRecommenderSystemNBOId;

            this.active = true;
            this.modal.show();
        } else {
            this._productRecommenderSystemNBOServiceProxy.getProductRecommenderSystemNBOForEdit(productRecommenderSystemNBOId).subscribe(result => {
                this.productRecommenderSystemNBO = result.productRecommenderSystemNBO;

                if (this.productRecommenderSystemNBO.lastTransactionDate) {
					this.lastTransactionDate = this.productRecommenderSystemNBO.lastTransactionDate.toDate();
                }

                this.active = true;
                this.modal.show();
            });
        }
    }

    save(): void {
            this.saving = true;

			
        if (this.lastTransactionDate) {
            if (!this.productRecommenderSystemNBO.lastTransactionDate) {
                this.productRecommenderSystemNBO.lastTransactionDate = moment(this.lastTransactionDate).startOf('day');
            }
            else {
                this.productRecommenderSystemNBO.lastTransactionDate = moment(this.lastTransactionDate);
            }
        }
        else {
            this.productRecommenderSystemNBO.lastTransactionDate = null;
        }
            this._productRecommenderSystemNBOServiceProxy.createOrEdit(this.productRecommenderSystemNBO)
             .pipe(finalize(() => { this.saving = false;}))
             .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
             });
    }







    close(): void {

        this.active = false;
        this.modal.hide();
    }
}
