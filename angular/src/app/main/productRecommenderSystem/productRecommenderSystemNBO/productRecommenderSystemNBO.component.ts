import {
    Component,
    Injector,
    ViewEncapsulation,
    ViewChild
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
    ProductRecommenderSystemNBOServiceProxy,
    ProductRecommenderSystemNBODto
} from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateOrEditProductRecommenderSystemNBOModalComponent } from './create-or-edit-productRecommenderSystemNBO-modal.component';
import { ViewProductRecommenderSystemNBOModalComponent } from './view-productRecommenderSystemNBO-modal.component';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Table } from 'primeng/components/table/table';
import { Paginator } from 'primeng/components/paginator/paginator';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
    templateUrl: './productRecommenderSystemNBO.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ProductRecommenderSystemNBOComponent extends AppComponentBase {
    @ViewChild('createOrEditProductRecommenderSystemNBOModal', { static: true })
    createOrEditProductRecommenderSystemNBOModal: CreateOrEditProductRecommenderSystemNBOModalComponent;
    @ViewChild('viewProductRecommenderSystemNBOModalComponent', {
        static: true
    })
    viewProductRecommenderSystemNBOModal: ViewProductRecommenderSystemNBOModalComponent;
    @ViewChild('entityTypeHistoryModal', { static: true })
    entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild('dataTable', { static: true }) dataTable: Table;
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    advancedFiltersAreShown = false;
    filterText = '';
    customerIDFilter = '';
    maxLastTransactionDateFilter: moment.Moment;
    // tslint:disable-next-line:indent
    minLastTransactionDateFilter: moment.Moment;
    numberOfdaysFilter = '';
    maxClustersFilter: number;
    maxClustersFilterEmpty: number;
    minClustersFilter: number;
    minClustersFilterEmpty: number;
    maxTransactionAmountFilter: number;
    maxTransactionAmountFilterEmpty: number;
    minTransactionAmountFilter: number;
    minTransactionAmountFilterEmpty: number;
    customerRegistrationLocationFilter = '';
    lastTransactionLocationFilter = '';
    maxProbabilityOfChurnFilter: number;
    maxProbabilityOfChurnFilterEmpty: number;
    minProbabilityOfChurnFilter: number;
    minProbabilityOfChurnFilterEmpty: number;
    morecliqFilter = -1;
    moretalkFilter = -1;
    moreflexFilter = -1;
    cliqliteFilter = -1;
    talkzoneFilter = -1;
    morelifeCompleteFilter = -1;
    morebusinessFilter = -1;
    morelifePostpaidFilter = -1;
    moreflexPostpaidFilter = -1;
    classicPostpaidFilter = -1;
    postpaidAccountBillPaymentFilter = '';
    postpaidBillBankPaymentFilter = -1;
    iddRatesFilter = -1;
    roamingInfoFilter = -1;
    roamingRatesFilter = -1;
    internationalGiftRechargeFilter = -1;
    umrahHajjRoamingFilter = -1;
    kwikcashFilter = -1;
    morecreditFilter = -1;
    automatedRechargeFilter = -1;
    moreWalletFilter = '';
    microInsuranceFilter = '';
    moresaversFilter = -1;
    morestatusFilter = -1;
    moretunezFilter = -1;
    corporateRbtFilter = -1;
    gamesFilter = -1;
    newsFilter = -1;
    daily25MBFilter = -1;
    daily100MBFilter = -1;
    weekly150MBFilter = -1;
    dnight1GBFilter = -1;
    mNight2GBFilter = -1;
    mNight5GBFilter = -1;
    yearly30GBFilter = -1;
    yearly60GBFilter = -1;
    yearly120GBFilter = -1;
    monthly500MBFilter = -1;
    monthly1GBFilter = -1;
    monthly1point5GBFilter = -1;
    monthly2point5GBFilter = -1;
    monthly4GBFilter = -1;
    monthly5point5GBFilter = -1;
    monthly7point1GBFilter = -1;
    monthly11point5GBFilter = -1;
    monthly15GBFilter = -1;
    monthly27point5GBFilter = -1;
    nextBestOffer1Filter = '';
    nextBestOffer2Filter = '';
    nextBestOffer3Filter = '';

    _entityTypeFullName =
        'TestDemo.ProductRecommenderSystem.ProductRecommenderSystemNBO';
    entityHistoryEnabled = false;

    constructor(
        injector: Injector,
        private _productRecommenderSystemNBOServiceProxy: ProductRecommenderSystemNBOServiceProxy,
        private _notifyService: NotifyService,
        private _tokenAuth: TokenAuthServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.entityHistoryEnabled = this.setIsEntityHistoryEnabled();
    }

    private setIsEntityHistoryEnabled(): boolean {
        let customSettings = (abp as any).custom;
        return (
            customSettings.EntityHistory &&
            customSettings.EntityHistory.isEnabled &&
            _.filter(
                customSettings.EntityHistory.enabledEntities,
                entityType => entityType === this._entityTypeFullName
            ).length === 1
        );
    }

    getProductRecommenderSystemNBO(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengTableHelper.showLoadingIndicator();

        this._productRecommenderSystemNBOServiceProxy
            .getAll(
                this.filterText,
                this.customerIDFilter,
                // this.maxLastTransactionDateFilter,
                // this.minLastTransactionDateFilter,
                // this.numberOfdaysFilter,
                // this.maxClustersFilter == null ? this.maxClustersFilterEmpty: this.maxClustersFilter,
                // this.minClustersFilter == null ? this.minClustersFilterEmpty: this.minClustersFilter,
                // this.maxTransactionAmountFilter == null ? this.maxTransactionAmountFilterEmpty: this.maxTransactionAmountFilter,
                // this.minTransactionAmountFilter == null ? this.minTransactionAmountFilterEmpty: this.minTransactionAmountFilter,
                // this.customerRegistrationLocationFilter,
                // this.lastTransactionLocationFilter,
                // this.maxProbabilityOfChurnFilter == null ? this.maxProbabilityOfChurnFilterEmpty: this.maxProbabilityOfChurnFilter,
                // this.minProbabilityOfChurnFilter == null ? this.minProbabilityOfChurnFilterEmpty: this.minProbabilityOfChurnFilter,
                // this.morecliqFilter,
                // this.moretalkFilter,
                // this.moreflexFilter,
                // this.cliqliteFilter,
                // this.talkzoneFilter,
                // this.morelifeCompleteFilter,
                // this.morebusinessFilter,
                // this.morelifePostpaidFilter,
                // this.moreflexPostpaidFilter,
                // this.classicPostpaidFilter,
                // this.postpaidAccountBillPaymentFilter,
                // this.postpaidBillBankPaymentFilter,
                // this.iddRatesFilter,
                // this.roamingInfoFilter,
                // this.roamingRatesFilter,
                // this.internationalGiftRechargeFilter,
                // this.umrahHajjRoamingFilter,
                // this.kwikcashFilter,
                // this.morecreditFilter,
                // this.automatedRechargeFilter,
                // this. moreWalletFilter,
                // this. microInsuranceFilter,
                // this.moresaversFilter,
                // this.morestatusFilter,
                // this.moretunezFilter,
                // this.corporateRbtFilter,
                // this.gamesFilter,
                // this.newsFilter,
                // this.daily25MBFilter,
                // this.daily100MBFilter,
                // this.weekly150MBFilter,
                // this.dnight1GBFilter,
                // this.mNight2GBFilter,
                // this.mNight5GBFilter,
                // this.yearly30GBFilter,
                // this.yearly60GBFilter,
                // this.yearly120GBFilter,
                // this.monthly500MBFilter,
                // this.monthly1GBFilter,
                // this.monthly1point5GBFilter,
                // this.monthly2point5GBFilter,
                // this.monthly4GBFilter,
                // this.monthly5point5GBFilter,
                // this.monthly7point1GBFilter,
                // this.monthly11point5GBFilter,
                // this.monthly15GBFilter,
                // this.monthly27point5GBFilter,
                // this.nextBestOffer1Filter,
                // this.nextBestOffer2Filter,
                // this.nextBestOffer3Filter,
                this.primengTableHelper.getSorting(this.dataTable),
                this.primengTableHelper.getSkipCount(this.paginator, event),
                this.primengTableHelper.getMaxResultCount(this.paginator, event)
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    createProductRecommenderSystemNBO(): void {
        this.createOrEditProductRecommenderSystemNBOModal.show();
    }

    showHistory(
        productRecommenderSystemNBO: ProductRecommenderSystemNBODto
    ): void {
        this.entityTypeHistoryModal.show({
            entityId: productRecommenderSystemNBO.id.toString(),
            entityTypeFullName: this._entityTypeFullName,
            entityTypeDescription: ''
        });
    }

    deleteProductRecommenderSystemNBO(
        productRecommenderSystemNBO: ProductRecommenderSystemNBODto
    ): void {
        this.message.confirm('', isConfirmed => {
            if (isConfirmed) {
                this._productRecommenderSystemNBOServiceProxy
                    .delete(productRecommenderSystemNBO.id)
                    .subscribe(() => {
                        this.reloadPage();
                        this.notify.success(this.l('SuccessfullyDeleted'));
                    });
            }
        });
    }

    exportToExcel(): void {
        this._productRecommenderSystemNBOServiceProxy
            .getProductRecommenderSystemNBOToExcel(
                this.filterText,
                this.customerIDFilter
                // this.maxLastTransactionDateFilter,
                // this.minLastTransactionDateFilter,
                // this.numberOfdaysFilter,
                // this.maxClustersFilter == null ? this.maxClustersFilterEmpty: this.maxClustersFilter,
                // this.minClustersFilter == null ? this.minClustersFilterEmpty: this.minClustersFilter,
                // this.maxTransactionAmountFilter == null ? this.maxTransactionAmountFilterEmpty: this.maxTransactionAmountFilter,
                // this.minTransactionAmountFilter == null ? this.minTransactionAmountFilterEmpty: this.minTransactionAmountFilter,
                // this.customerRegistrationLocationFilter,
                // this.lastTransactionLocationFilter,
                // this.maxProbabilityOfChurnFilter == null ? this.maxProbabilityOfChurnFilterEmpty: this.maxProbabilityOfChurnFilter,
                // this.minProbabilityOfChurnFilter == null ? this.minProbabilityOfChurnFilterEmpty: this.minProbabilityOfChurnFilter,
                // this.morecliqFilter,
                // this.moretalkFilter,
                // this.moreflexFilter,
                // this.cliqliteFilter,
                // this.talkzoneFilter,
                // this.morelifeCompleteFilter,
                // this.morebusinessFilter,
                // this.morelifePostpaidFilter,
                // this.moreflexPostpaidFilter,
                // this.classicPostpaidFilter,
                // this.postpaidAccountBillPaymentFilter,
                // this.postpaidBillBankPaymentFilter,
                // this.iddRatesFilter,
                // this.roamingInfoFilter,
                // this.roamingRatesFilter,
                // this.internationalGiftRechargeFilter,
                // this.umrahHajjRoamingFilter,
                // this.kwikcashFilter,
                // this.morecreditFilter,
                // this.automatedRechargeFilter,
                // this. moreWalletFilter,
                // this. microInsuranceFilter,
                // this.moresaversFilter,
                // this.morestatusFilter,
                // this.moretunezFilter,
                // this.corporateRbtFilter,
                // this.gamesFilter,
                // this.newsFilter,
                // this.daily25MBFilter,
                // this.daily100MBFilter,
                // this.weekly150MBFilter,
                // this.dnight1GBFilter,
                // this.mNight2GBFilter,
                // this.mNight5GBFilter,
                // this.yearly30GBFilter,
                // this.yearly60GBFilter,
                // this.yearly120GBFilter,
                // this.monthly500MBFilter,
                // this.monthly1GBFilter,
                // this.monthly1point5GBFilter,
                // this.monthly2point5GBFilter,
                // this.monthly4GBFilter,
                // this.monthly5point5GBFilter,
                // this.monthly7point1GBFilter,
                // this.monthly11point5GBFilter,
                // this.monthly15GBFilter,
                // this.monthly27point5GBFilter,
                // this.nextBestOffer1Filter,
                // this.nextBestOffer2Filter,
                // this.nextBestOffer3Filter,
            )
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
