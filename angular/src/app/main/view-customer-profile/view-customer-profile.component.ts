import { AppComponentBase } from '@shared/common/app-component-base';
import { Component, OnInit, ViewEncapsulation, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view-customer-profile',
  templateUrl: './view-customer-profile.component.html',
  styleUrls: ['./view-customer-profile.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [appModuleAnimation()]
})
export class ViewCustomerProfileComponent extends AppComponentBase implements OnInit {

  fake_customer_data: Customers = new Customers();
  _random_customer_index = Math.floor(Math.random() * 5);

  customerPicture = '';
  customerName = '';
  customerID = '';
  customerBVN = '';
  customerDOB = '';
  gender = '';
  savings_acct = '';
  current_acct = '';
  email = '';
  phone = '';
  address = '';
  device = {count: 0, trans: 0, amt: 0};
  ben = {count: 0, trans: 0, amt: 0};
  bill = {count: 0, trans: 0, amt: 0};
  top3_channels = [];
  top3_devices = [];
  top3_bens = [];
  top3_merchants = [];
  savings_acct_bal = 0;
  current_acct_bal = 0;

  constructor(
    injector: Injector,
    private _location: Location) {
      super(injector);
     }

  ngOnInit() {
    this._random_customer_index = Math.floor(Math.random() * 5);
    console.log(this._random_customer_index);

    this.customerPicture = this.fake_customer_data.customerPics[this._random_customer_index];
    this.customerName = this.fake_customer_data.customerNames[this._random_customer_index];
    this.customerID = this.fake_customer_data.customerIDs[this._random_customer_index];
    this.customerBVN = this.fake_customer_data.bvns[this._random_customer_index];
    this.customerDOB = this.fake_customer_data.dobs[this._random_customer_index];
    this.gender = this.fake_customer_data.gender[this._random_customer_index];
    this.savings_acct = this.fake_customer_data.savings_acct[this._random_customer_index];
    this.current_acct = this.fake_customer_data.current_acct[this._random_customer_index];
    this.email = this.fake_customer_data.emails[this._random_customer_index];
    this.phone = this.fake_customer_data.phones[this._random_customer_index];
    this.address = this.fake_customer_data.addresses[this._random_customer_index];
    this.device = this.fake_customer_data.devices[this._random_customer_index];
    this.ben = this.fake_customer_data.ben[this._random_customer_index];
    this.bill = this.fake_customer_data.bills[this._random_customer_index];
    this.top3_channels = this.fake_customer_data.top3Channels[this._random_customer_index];
    this.top3_devices = this.fake_customer_data.top3devices[this._random_customer_index];
    this.top3_bens = this.fake_customer_data.top3Bens[this._random_customer_index];
    this.top3_merchants = this.fake_customer_data.top3Merchants[this._random_customer_index];

    this.savings_acct_bal = Math.floor(Math.random() * 100000) + 50000;
    this.current_acct_bal = Math.floor(Math.random() * 100000) + 100000;
  }

  goBack() {
    this._location.back();
  }

}

class Customers {
  customerPics = ['59.jpg', '80.jpg', '91.jpg', '92.jpg', '89.jpg'];
  customerNames = ['Chukwuma John', 'James Folarin', 'Oguntunde Michael', 'Seun Olajide', 'Fatima James'];
  customerIDs = ['005311972', '0094873652', '009484725', '001948657', '0067389123'];
  bvns = ['0809 000 0200', '0809 000 0201', '0809 000 0202', '0809 000 0203', '0809 000 0204'];
  dobs = ['Apr 08, 1985', 'Jun 18, 1985', 'Aug 19, 1985', 'Sept 08, 1985', 'Dec 08, 1985'];
  gender = ['Male', 'Male', 'Male', 'Female', 'Female'];
  savings_acct = ['0039564782', '00220938456', '0011265342', '0015647880', '0012944330'];
  current_acct = ['0026374782', '0033465783', '0083746501', '0077225342', '0015447830'];
  emails = ['chukwuma.john@gmail.com', 'james.folarin@gmail.com', 'omichaek@gmail.com', 'seun.olajide@gmail.com', 'fatima.j@gmail.com'];
  phones = ['08015678920', '08033644920', '08023678400', '08078679480', '08015948920'];
  devices = [
    {count: 3, trans: 192, amt: 550},
    {count: 1, trans: 92, amt: 250},
    {count: 2, trans: 475, amt: 850},
    {count: 3, trans: 342, amt: 420},
    {count: 1, trans: 110, amt: 340}
  ];
  ben = [
    {count: 15, trans: 101, amt: 331},
    {count: 5, trans: 92, amt: 250},
    {count: 20, trans: 343, amt: 671},
    {count: 10, trans: 202, amt: 220},
    {count: 1, trans: 82, amt: 300}
  ];
  bills = [
    {count: 10, trans: 91, amt: 219},
    {count: 0, trans: 0, amt: 0},
    {count: 13, trans: 132, amt: 179},
    {count: 7, trans: 140, amt: 200},
    {count: 2, trans: 10, amt: 40}
  ];
  addresses = [
    '1, Ajose Adeogun<br> Victoria Island<br> Lagos',
    '22, Jasojo Street<br> Ifako-ijaiye<br> Lagos',
    '10, Ogundele Street<br> Abule-egba<br> Lagos',
    '15, Ademola Street,<br> Oniru<br> Lagos',
    '105b, Okeniji-jademi Street<br> Victoria Island<br> Lagos'
  ];
  top3Channels = [
    [
      {channel: 'Voice', transType: 'Voice Call', count: 102, total: 352000, freq: 'Monthly'},
      {channel: 'USSD', transType: 'Recharge', count: 27, total: 148000, freq: 'Monthly'},
      {channel: 'Data', transType: 'Games', count: 63, total: 50500, freq: 'Monthly'}
    ],
    [
      {channel: 'Voice', transType: 'Voice Call', count: 40, total: 200000, freq: 'Monthly'},
      {channel: 'USSD', transType: 'Recharge', count: 38, total: 30000, freq: 'Monthly'},
      {channel: 'Data', transType: 'Games', count: 24, total: 20000, freq: 'Monthly'}
    ],
    [
      {channel: 'Voice', transType: 'Voice Call', count: 325, total: 500000, freq: 'Weekly'},
      {channel: 'USSD', transType: 'Recharge', count: 100, total: 300000, freq: 'Monthly'},
      {channel: 'Data', transType: 'Games', count: 25, total: 50000, freq: 'Monthly'}
    ],
    [
      {channel: 'Voice', transType: 'Voice Call', count: 122, total: 320000, freq: 'Weekly'},
      {channel: 'Data', transType: 'Data Call', count: 120, total: 75000, freq: 'Monthly'},
      {channel: 'USSD', transType: 'Games', count: 100, total: 25000, freq: 'Monthly'}
    ],
    [
      {channel: 'Voice', transType: 'Voice Call', count: 75, total: 500000, freq: 'Weekly'},
      {channel: 'USSD', transType: 'Recharge', count: 35, total: 300000, freq: 'Monthly'}
    ],
  ];
  top3devices = [
    [
      {device: 'I-phone X', os: 'IOS', count: 105, total: 290000, freq: 'Daily'},
      {device: 'Samsung s6', os: 'Android', count: 60, total: 210000, freq: 'Weekly'},
      {device: 'LAPTOP HTVL809', os: 'Windows 10', count: 27, total: 50000, freq: 'Monthly'},
    ],
    [
      {device: 'Tecno-Camon CX', os: 'Android', count: 92, total: 250000, freq: 'Daily'},
    ],
    [
      {device: 'I-phone X', os: 'IOS', count: 375, total: 560000, freq: 'Daily'},
      {device: 'Ipad-Pro', os: 'IOS', count: 100, total: 310000, freq: 'Weekly'},
    ],
    [
      {device: 'I-phone 6', os: 'IOS', count: 202, total: 220000, freq: 'Daily'},
      {device: 'Samsung Tablet', os: 'Android', count: 100, total: 150000, freq: 'Weekly'},
      {device: 'Lenove T470', os: 'Windows 10', count: 40, total: 50000, freq: 'Monthly'},
    ],
    [
      {device: 'I-phone 5', os: 'IOS', count: 110, total: 340000, freq: 'Daily'}
    ],
  ];
  top3Bens = [
    [
      {name: 'Voice', bank: 'talkzone', count: 'moretalk', total: 'morecliq', freq: 'Weekly'},
      {name: 'Data', bank: 'Monthly 4GB', count: 'news', total: 'international gift recharge', freq: 'Monthly'},
      {name: 'Others', bank: 'kwikcash', count: 'morecredit', total: 'automated recharge', freq: 'Monthly'},
    ],
    [
      {name: 'Voice', bank: 'talkzone', count: 'moretalk', total: 'morecliq', freq: 'Weekly'},
      {name: 'Data', bank: 'Monthly 4GB', count: 'news', total: 'international gift recharge', freq: 'Monthly'},
      {name: 'Others', bank: 'kwikcash', count: 'morecredit', total: 'automated recharge', freq: 'Monthly'},
    ],
    [
      {name: 'Voice', bank: 'talkzone', count: 'moretalk', total: 'morecliq', freq: 'Weekly'},
      {name: 'Data', bank: 'Monthly 4GB', count: 'news', total: 'international gift recharge', freq: 'Monthly'},
      {name: 'Others', bank: 'kwikcash', count: 'morecredit', total: 'automated recharge', freq: 'Monthly'},
    ],
    [
      {name: 'Voice', bank: 'talkzone', count: 'moretalk', total: 'morecliq', freq: 'Weekly'},
      {name: 'Data', bank: 'Monthly 4GB', count: 'news', total: 'international gift recharge', freq: 'Monthly'},
      {name: 'Others', bank: 'kwikcash', count: 'morecredit', total: 'automated recharge', freq: 'Monthly'},
    ],
    [
      {name: 'Voice', bank: 'talkzone', count: 'moretalk', total: 'morecliq', freq: 'Weekly'},
      {name: 'Data', bank: 'Monthly 4GB', count: 'news', total: 'international gift recharge', freq: 'Monthly'},
      {name: 'Others', bank: 'kwikcash', count: 'morecredit', total: 'automated recharge', freq: 'Monthly'},
    ],
  ];
  top3Merchants = [
    [
      {name: 'Jumia.com.ng', cat: 'Online shopping', count: 7, total: 30000, freq: 'Monthly'},
      {name: 'DSTV', cat: 'Cable / TV', count: 3, total: 12000, freq: 'Monthly'},
      {name: 'Swift Networks', cat: 'Internet Subscription', count: 3, total: 10500, freq: 'Monthly'},
    ],
    [
      {name: 'None', cat: '', count: 1, total: 1, freq: ''},
    ],
    [
      {name: 'Jumia.com.ng', cat: 'Online shopping', count: 7, total: 30000, freq: 'Monthly'},
      {name: 'DSTV', cat: 'Cable / TV', count: 3, total: 12000, freq: 'Monthly'},
      {name: 'Smile Networks', cat: 'Internet Subscription', count: 3, total: 10500, freq: 'Monthly'},
    ],
    [
      {name: 'Konga.com', cat: 'Online shopping', count: 7, total: 30000, freq: 'Monthly'},
      {name: 'DSTV', cat: 'Cable / TV', count: 3, total: 12000, freq: 'Monthly'},
      {name: 'Spetranet Networks', cat: 'Internet Subscription', count: 3, total: 10500, freq: 'Monthly'},
    ],
    [
      {name: 'Jumia.com.ng', cat: 'Online shopping', count: 7, total: 30000, freq: 'Monthly'},
      {name: 'DSTV', cat: 'Cable / TV', count: 3, total: 12000, freq: 'Monthly'},
    ],
  ];
}
