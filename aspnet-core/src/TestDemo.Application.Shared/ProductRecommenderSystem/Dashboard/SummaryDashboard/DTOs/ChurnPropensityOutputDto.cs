﻿

using System.Collections.Generic;

namespace TestDemo.ProductRecommenderSystem.Dashboard.SummaryDashboard.DTOs
{
    public class ChurnPropensityOutputDto
    {
        public int churnCustomerCount { get; set; }

        public string churnStatus { get; set; }

        public string churnSegment { get; set; }

        public string churnSegment2 { get; set; }

        public string churnSegment3 { get; set; }

        public List<ChurntableDTO> CustomerList { get; set; }

    }

    public class ChurntableDTO
    {
        public string customerID { get; set; }
        public string NBO1 { get; set; }

        public string NBO2 { get; set; }

        public string NBO3 { get; set; }

        public string customerSegment { get; set; }
    }
}
