﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDemo.ProductRecommenderSystem.Dashboard.SummaryDashboard.DTOs
{
    public class CustomerLocation1Dto
    {
        public double latitude { get; set; }

        public double longitude { get; set; }
    }
}
