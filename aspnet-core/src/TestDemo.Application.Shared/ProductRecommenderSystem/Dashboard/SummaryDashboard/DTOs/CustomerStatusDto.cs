﻿using System;

namespace TestDemo.ProductRecommenderSystem.Dashboard.SummaryDashboard.DTOs
{
    class CustomerStatusDto
    {
        public string CustomerID { get; set; }

        public double AmountSpent { get; set; }

        public DateTime lastTransactionDate { get; set; }

        public string NBO1 { get; set; }

        public string NBO2 { get; set; }

        public string NBO3 { get; set; }

        public string CustomerSegment { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }


        public string churnStatus { get; set; }
    }
}
