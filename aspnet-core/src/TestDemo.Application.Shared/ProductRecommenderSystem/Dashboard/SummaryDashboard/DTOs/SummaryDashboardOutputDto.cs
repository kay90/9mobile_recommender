﻿namespace TestDemo.ProductRecommenderSystem.Dashboard.SummaryDashboard.DTOs
{
    public class SummaryDashboardOutputDto
    {
        public double lowPropensity { get; set; }

        public double mediumLowPropensity { get; set; }

        public double mediumHighPropensity { get; set; }

        public double highPropensity { get; set; }

        public string firstNextBestOffer { get; set; }

        public string secondNextBestOffer { get; set; }

        public string thirdNextBestOffer { get; set; }

        public string lastTransactionLocation { get; set; }

    }
    public class CustomerLocationDto
    {
        public double latitude { get; set; }

        public double longitude { get; set; }
    }

}
