using Abp.Application.Services.Dto;

namespace TestDemo.ProductRecommenderSystem.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}