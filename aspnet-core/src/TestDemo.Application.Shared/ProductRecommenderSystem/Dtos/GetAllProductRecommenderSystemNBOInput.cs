using Abp.Application.Services.Dto;
using System;

namespace TestDemo.ProductRecommenderSystem.Dtos
{
    public class GetAllProductRecommenderSystemNBOInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }

		public string CustomerIDFilter { get; set; }

		//public DateTime? MaxLastTransactionDateFilter { get; set; }
		//public DateTime? MinLastTransactionDateFilter { get; set; }

		//public string NumberOfdaysFilter { get; set; }

		//public int? MaxClustersFilter { get; set; }
		//public int? MinClustersFilter { get; set; }

		//public double? MaxTransactionAmountFilter { get; set; }
		//public double? MinTransactionAmountFilter { get; set; }

		//public string CustomerRegistrationLocationFilter { get; set; }

		//public string LastTransactionLocationFilter { get; set; }

		//public double? MaxProbabilityOfChurnFilter { get; set; }
		//public double? MinProbabilityOfChurnFilter { get; set; }

		//public bool morecliqFilter { get; set; }

		//public bool moretalkFilter { get; set; }

		//public bool moreflexFilter { get; set; }

		//public bool cliqliteFilter { get; set; }

		//public bool talkzoneFilter { get; set; }

		//public bool morelifeCompleteFilter { get; set; }

		//public bool morebusinessFilter { get; set; }

		//public bool morelifePostpaidFilter { get; set; }

		//public bool moreflexPostpaidFilter { get; set; }

		//public bool classicPostpaidFilter { get; set; }

		//public bool postpaidAccountBillPaymentFilter { get; set; }

		//public bool postpaidBillBankPaymentFilter { get; set; }

		//public bool IDDRatesFilter { get; set; }

		//public bool roamingInfoFilter { get; set; }

		//public bool roamingRatesFilter { get; set; }

		//public bool internationalGiftRechargeFilter { get; set; }

		//public bool umrahHajjRoamingFilter { get; set; }

		//public bool kwikcashFilter { get; set; }

		//public bool morecreditFilter { get; set; }

		//public bool automatedRechargeFilter { get; set; }

		//public bool  moreWalletFilter { get; set; }

		//public bool  microInsuranceFilter { get; set; }

		//public bool moresaversFilter { get; set; }

		//public bool morestatusFilter { get; set; }

		//public bool moretunezFilter { get; set; }

		//public bool corporateRbtFilter { get; set; }

		//public bool gamesFilter { get; set; }

		//public bool newsFilter { get; set; }

		//public bool daily25MBFilter { get; set; }

		//public bool daily100MBFilter { get; set; }

		//public bool weekly150MBFilter { get; set; }

		//public bool Dnight1GBFilter { get; set; }

		//public bool MNight2GBFilter { get; set; }

		//public bool MNight5GBFilter { get; set; }

		//public bool yearly30GBFilter { get; set; }

		//public bool yearly60GBFilter { get; set; }

		//public bool yearly120GBFilter { get; set; }

		//public bool Monthly500MBFilter { get; set; }

		//public bool Monthly1GBFilter { get; set; }

		//public bool Monthly1point5GBFilter { get; set; }

		//public bool Monthly2point5GBFilter { get; set; }

		//public bool Monthly4GBFilter { get; set; }

		//public bool Monthly5point5GBFilter { get; set; }

		//public bool Monthly7point1GBFilter { get; set; }

		//public bool Monthly11point5GBFilter { get; set; }

		//public bool Monthly15GBFilter { get; set; }

		//public bool Monthly27point5GBFilter { get; set; }

		//public string NextBestOffer1Filter { get; set; }

		//public string NextBestOffer2Filter { get; set; }

		//public string NextBestOffer3Filter { get; set; }



    }
}