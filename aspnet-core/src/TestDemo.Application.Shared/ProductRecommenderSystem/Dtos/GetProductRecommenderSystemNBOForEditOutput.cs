using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace TestDemo.ProductRecommenderSystem.Dtos
{
    public class GetProductRecommenderSystemNBOForEditOutput
    {
		public CreateOrEditProductRecommenderSystemNBODto ProductRecommenderSystemNBO { get; set; }


    }
}