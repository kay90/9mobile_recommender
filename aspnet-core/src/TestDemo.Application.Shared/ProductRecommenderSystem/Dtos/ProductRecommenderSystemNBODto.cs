
using System;
using Abp.Application.Services.Dto;

namespace TestDemo.ProductRecommenderSystem.Dtos
{
    public class ProductRecommenderSystemNBODto : EntityDto
    {
		public string CustomerID { get; set; }

		public DateTime? LastTransactionDate { get; set; }

		public string NumberOfdays { get; set; }

		public int? Clusters { get; set; }

		public double? TransactionAmount { get; set; }

		public string CustomerRegistrationLocation { get; set; }

		public string LastTransactionLocation { get; set; }

		public double? ProbabilityOfChurn { get; set; }

		public bool morecliq { get; set; }

		public bool moretalk { get; set; }

		public bool moreflex { get; set; }

		public bool cliqlite { get; set; }

		public bool talkzone { get; set; }

		public bool morelifeComplete { get; set; }

		public bool morebusiness { get; set; }

		public bool morelifePostpaid { get; set; }

		public bool moreflexPostpaid { get; set; }

		public bool classicPostpaid { get; set; }

		public bool postpaidAccountBillPayment { get; set; }

		public bool postpaidBillBankPayment { get; set; }

		public bool IDDRates { get; set; }

		public bool roamingInfo { get; set; }

		public bool roamingRates { get; set; }

		public bool internationalGiftRecharge { get; set; }

		public bool umrahHajjRoaming { get; set; }

		public bool kwikcash { get; set; }

		public bool morecredit { get; set; }

		public bool automatedRecharge { get; set; }

		public bool  moreWallet { get; set; }

		public bool  microInsurance { get; set; }

		public bool moresavers { get; set; }

		public bool morestatus { get; set; }

		public bool moretunez { get; set; }

		public bool corporateRbt { get; set; }

		public bool games { get; set; }

		public bool news { get; set; }

		public bool daily25MB { get; set; }

		public bool daily100MB { get; set; }

		public bool weekly150MB { get; set; }

		public bool Dnight1GB { get; set; }

		public bool MNight2GB { get; set; }

		public bool MNight5GB { get; set; }

		public bool yearly30GB { get; set; }

		public bool yearly60GB { get; set; }

		public bool yearly120GB { get; set; }

		public bool Monthly500MB { get; set; }

		public bool Monthly1GB { get; set; }

		public bool Monthly1point5GB { get; set; }

		public bool Monthly2point5GB { get; set; }

		public bool Monthly4GB { get; set; }

		public bool Monthly5point5GB { get; set; }

		public bool Monthly7point1GB { get; set; }

		public bool Monthly11point5GB { get; set; }

		public bool Monthly15GB { get; set; }

		public bool Monthly27point5GB { get; set; }

		public string NextBestOffer1 { get; set; }

		public string NextBestOffer2 { get; set; }

		public string NextBestOffer3 { get; set; }



    }
}