using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TestDemo.ProductRecommenderSystem.Dtos;
using TestDemo.Dto;

namespace TestDemo.ProductRecommenderSystem
{
    public interface IProductRecommenderSystemNBOAppService : IApplicationService 
    {
        Task<PagedResultDto<GetProductRecommenderSystemNBOForViewDto>> GetAll(GetAllProductRecommenderSystemNBOInput input);

        Task<GetProductRecommenderSystemNBOForViewDto> GetProductRecommenderSystemNBOForView(int id);

		Task<GetProductRecommenderSystemNBOForEditOutput> GetProductRecommenderSystemNBOForEdit(EntityDto input);

		Task CreateOrEdit(CreateOrEditProductRecommenderSystemNBODto input);

		Task Delete(EntityDto input);

		Task<FileDto> GetProductRecommenderSystemNBOToExcel(GetAllProductRecommenderSystemNBOForExcelInput input);

		
    }
}