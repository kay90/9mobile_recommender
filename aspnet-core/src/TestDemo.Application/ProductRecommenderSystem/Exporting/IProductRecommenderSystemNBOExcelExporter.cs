using System.Collections.Generic;
using TestDemo.ProductRecommenderSystem.Dtos;
using TestDemo.Dto;

namespace TestDemo.ProductRecommenderSystem.Exporting
{
    public interface IProductRecommenderSystemNBOExcelExporter
    {
        FileDto ExportToFile(List<GetProductRecommenderSystemNBOForViewDto> productRecommenderSystemNBO);
    }
}