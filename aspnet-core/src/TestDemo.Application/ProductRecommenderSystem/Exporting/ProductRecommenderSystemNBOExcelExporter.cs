using System.Collections.Generic;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using TestDemo.DataExporting.Excel.EpPlus;
using TestDemo.ProductRecommenderSystem.Dtos;
using TestDemo.Dto;
using TestDemo.Storage;

namespace TestDemo.ProductRecommenderSystem.Exporting
{
    public class ProductRecommenderSystemNBOExcelExporter : EpPlusExcelExporterBase, IProductRecommenderSystemNBOExcelExporter
    {

        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ProductRecommenderSystemNBOExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
			ITempFileCacheManager tempFileCacheManager) :  
	base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<GetProductRecommenderSystemNBOForViewDto> productRecommenderSystemNBO)
        {
            return CreateExcelPackage(
                "ProductRecommenderSystemNBO.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ProductRecommenderSystemNBO"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CustomerID"),
                        L("LastTransactionDate"),
                        L("NumberOfdays"),
                        L("Clusters"),
                        L("TransactionAmount"),
                        L("CustomerRegistrationLocation"),
                        L("LastTransactionLocation"),
                        L("ProbabilityOfChurn"),
                        L("morecliq"),
                        L("moretalk"),
                        L("moreflex"),
                        L("cliqlite"),
                        L("talkzone"),
                        L("morelifeComplete"),
                        L("morebusiness"),
                        L("morelifePostpaid"),
                        L("moreflexPostpaid"),
                        L("classicPostpaid"),
                        L("postpaidAccountBillPayment"),
                        L("postpaidBillBankPayment"),
                        L("IDDRates"),
                        L("roamingInfo"),
                        L("roamingRates"),
                        L("internationalGiftRecharge"),
                        L("umrahHajjRoaming"),
                        L("kwikcash"),
                        L("morecredit"),
                        L("automatedRecharge"),
                        L(" moreWallet"),
                        L(" microInsurance"),
                        L("moresavers"),
                        L("morestatus"),
                        L("moretunez"),
                        L("corporateRbt"),
                        L("games"),
                        L("news"),
                        L("daily25MB"),
                        L("daily100MB"),
                        L("weekly150MB"),
                        L("Dnight1GB"),
                        L("MNight2GB"),
                        L("MNight5GB"),
                        L("yearly30GB"),
                        L("yearly60GB"),
                        L("yearly120GB"),
                        L("Monthly500MB"),
                        L("Monthly1GB"),
                        L("Monthly1point5GB"),
                        L("Monthly2point5GB"),
                        L("Monthly4GB"),
                        L("Monthly5point5GB"),
                        L("Monthly7point1GB"),
                        L("Monthly11point5GB"),
                        L("Monthly15GB"),
                        L("Monthly27point5GB"),
                        L("NextBestOffer1"),
                        L("NextBestOffer2"),
                        L("NextBestOffer3")
                        );

                    AddObjects(
                        sheet, 2, productRecommenderSystemNBO,
                        _ => _.ProductRecommenderSystemNBO.CustomerID,
                        _ => _timeZoneConverter.Convert(_.ProductRecommenderSystemNBO.LastTransactionDate, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.ProductRecommenderSystemNBO.NumberOfdays,
                        _ => _.ProductRecommenderSystemNBO.Clusters,
                        _ => _.ProductRecommenderSystemNBO.TransactionAmount,
                        _ => _.ProductRecommenderSystemNBO.CustomerRegistrationLocation,
                        _ => _.ProductRecommenderSystemNBO.LastTransactionLocation,
                        _ => _.ProductRecommenderSystemNBO.ProbabilityOfChurn,
                        _ => _.ProductRecommenderSystemNBO.morecliq,
                        _ => _.ProductRecommenderSystemNBO.moretalk,
                        _ => _.ProductRecommenderSystemNBO.moreflex,
                        _ => _.ProductRecommenderSystemNBO.cliqlite,
                        _ => _.ProductRecommenderSystemNBO.talkzone,
                        _ => _.ProductRecommenderSystemNBO.morelifeComplete,
                        _ => _.ProductRecommenderSystemNBO.morebusiness,
                        _ => _.ProductRecommenderSystemNBO.morelifePostpaid,
                        _ => _.ProductRecommenderSystemNBO.moreflexPostpaid,
                        _ => _.ProductRecommenderSystemNBO.classicPostpaid,
                        _ => _.ProductRecommenderSystemNBO.postpaidAccountBillPayment,
                        _ => _.ProductRecommenderSystemNBO.postpaidBillBankPayment,
                        _ => _.ProductRecommenderSystemNBO.IDDRates,
                        _ => _.ProductRecommenderSystemNBO.roamingInfo,
                        _ => _.ProductRecommenderSystemNBO.roamingRates,
                        _ => _.ProductRecommenderSystemNBO.internationalGiftRecharge,
                        _ => _.ProductRecommenderSystemNBO.umrahHajjRoaming,
                        _ => _.ProductRecommenderSystemNBO.kwikcash,
                        _ => _.ProductRecommenderSystemNBO.morecredit,
                        _ => _.ProductRecommenderSystemNBO.automatedRecharge,
                        _ => _.ProductRecommenderSystemNBO. moreWallet,
                        _ => _.ProductRecommenderSystemNBO. microInsurance,
                        _ => _.ProductRecommenderSystemNBO.moresavers,
                        _ => _.ProductRecommenderSystemNBO.morestatus,
                        _ => _.ProductRecommenderSystemNBO.moretunez,
                        _ => _.ProductRecommenderSystemNBO.corporateRbt,
                        _ => _.ProductRecommenderSystemNBO.games,
                        _ => _.ProductRecommenderSystemNBO.news,
                        _ => _.ProductRecommenderSystemNBO.daily25MB,
                        _ => _.ProductRecommenderSystemNBO.daily100MB,
                        _ => _.ProductRecommenderSystemNBO.weekly150MB,
                        _ => _.ProductRecommenderSystemNBO.Dnight1GB,
                        _ => _.ProductRecommenderSystemNBO.MNight2GB,
                        _ => _.ProductRecommenderSystemNBO.MNight5GB,
                        _ => _.ProductRecommenderSystemNBO.yearly30GB,
                        _ => _.ProductRecommenderSystemNBO.yearly60GB,
                        _ => _.ProductRecommenderSystemNBO.yearly120GB,
                        _ => _.ProductRecommenderSystemNBO.Monthly500MB,
                        _ => _.ProductRecommenderSystemNBO.Monthly1GB,
                        _ => _.ProductRecommenderSystemNBO.Monthly1point5GB,
                        _ => _.ProductRecommenderSystemNBO.Monthly2point5GB,
                        _ => _.ProductRecommenderSystemNBO.Monthly4GB,
                        _ => _.ProductRecommenderSystemNBO.Monthly5point5GB,
                        _ => _.ProductRecommenderSystemNBO.Monthly7point1GB,
                        _ => _.ProductRecommenderSystemNBO.Monthly11point5GB,
                        _ => _.ProductRecommenderSystemNBO.Monthly15GB,
                        _ => _.ProductRecommenderSystemNBO.Monthly27point5GB,
                        _ => _.ProductRecommenderSystemNBO.NextBestOffer1,
                        _ => _.ProductRecommenderSystemNBO.NextBestOffer2,
                        _ => _.ProductRecommenderSystemNBO.NextBestOffer3
                        );

					var lastTransactionDateColumn = sheet.Column(2);
                    lastTransactionDateColumn.Style.Numberformat.Format = "yyyy-mm-dd";
					lastTransactionDateColumn.AutoFit();
					

                });
        }
    }
}
