

using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using TestDemo.ProductRecommenderSystem.Exporting;
using TestDemo.ProductRecommenderSystem.Dtos;
using TestDemo.Dto;
using Abp.Application.Services.Dto;
using TestDemo.Authorization;
using Abp.Extensions;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using TestDemo.ProductRecommenderSystem.Dashboard.SummaryDashboard.DTOs;

namespace TestDemo.ProductRecommenderSystem
{
	[AbpAuthorize(AppPermissions.Pages_ProductRecommenderSystemNBO)]
    public class ProductRecommenderSystemNBOAppService : TestDemoAppServiceBase, IProductRecommenderSystemNBOAppService
    {
		 private readonly IRepository<ProductRecommenderSystemNBO> _productRecommenderSystemNBORepository;
		 private readonly IProductRecommenderSystemNBOExcelExporter _productRecommenderSystemNBOExcelExporter;
		 

		  public ProductRecommenderSystemNBOAppService(IRepository<ProductRecommenderSystemNBO> productRecommenderSystemNBORepository, IProductRecommenderSystemNBOExcelExporter productRecommenderSystemNBOExcelExporter ) 
		  {
			_productRecommenderSystemNBORepository = productRecommenderSystemNBORepository;
			_productRecommenderSystemNBOExcelExporter = productRecommenderSystemNBOExcelExporter;
			
		  }
           
            public async Task<SummaryDashboardOutputDto> GetSummaryDashboardDetails()
         {
            var Dto = new SummaryDashboardOutputDto
            {
                lowPropensity = await _productRecommenderSystemNBORepository.CountAsync(
                    x => x.ProbabilityOfChurn < 0.25
                    ),
                mediumLowPropensity = await _productRecommenderSystemNBORepository.CountAsync(
                    x => x.ProbabilityOfChurn >= 0.25 && x.ProbabilityOfChurn <= 0.49
                    ),
                mediumHighPropensity = await _productRecommenderSystemNBORepository.CountAsync(
                    x => x.ProbabilityOfChurn >= 0.5 && x.ProbabilityOfChurn <= 0.74
                    ),
                highPropensity = await _productRecommenderSystemNBORepository.CountAsync(
                    x => x.ProbabilityOfChurn >= 0.75
                    ),
                firstNextBestOffer = GetNBO1(),
                secondNextBestOffer = GetNBO2(),
                thirdNextBestOffer = GetNBO3(),

            };

            return Dto;
        }


        public String GetNBO1()
        {
            var NBO1Array = _productRecommenderSystemNBORepository.GetAll()
                                    .GroupBy(x => x.NextBestOffer1)
                                    .Select(x => new { nbo = x.Key, count = x.Count() })
                                    .OrderByDescending(x => x.count)
                                    .Take(3)
                                    .ToList();


            return NBO1Array[0].nbo;          
        }


        public String GetNBO2()
        {
            var NBO2Array = _productRecommenderSystemNBORepository.GetAll()
                                    .GroupBy(x => x.NextBestOffer2)
                                    .Select(x => new { nbo = x.Key, count = x.Count() })
                                    .OrderByDescending(x => x.count)
                                    .Take(3)
                                    .ToList();
            var NB01 = GetNBO1();

            if (NBO2Array[0].nbo == NB01)
            {
                return NBO2Array[1].nbo;
            }
            else
            {
               return  NBO2Array[0].nbo;
            }

        }


        public String GetNBO3()
        {
            string temp = String.Empty;
            var NBO3Array = _productRecommenderSystemNBORepository.GetAll()
                                    .GroupBy(x => x.NextBestOffer3)
                                    .Select(x => new { nbo = x.Key, count = x.Count() })
                                    .OrderByDescending(x => x.count)
                                    .Take(3)
                                    .ToList();
            string NB01 = GetNBO1();
            string NB02 = GetNBO2();

            if ((NBO3Array[0].nbo == NB01) || (NBO3Array[0].nbo == NB02))
            {
                if ((NBO3Array[1].nbo == NB01) || (NBO3Array[1].nbo == NB02))
                        {
                    temp = NBO3Array[2].nbo;
                }
            }
            else
            {
                temp = NBO3Array[0].nbo;
            }
            return temp;
        }


        //public async Task<CustomerLocationDto> GetLocationDetails()
        //{
        //    _productRecommenderSystemNBORepository = productRecommenderSystemNBORepository;
        //    var LocationDto = new CustomerLocationDto
        //}

        public async Task<ChurnPropensityOutputDto>GetAllChurnDetails(string churnStatus)
        {

            var filteredChurnDto = _productRecommenderSystemNBORepository.GetAll()
                .WhereIf(churnStatus == "LP", x => x.ProbabilityOfChurn < 0.25)
                .WhereIf(churnStatus == "MP", x => x.ProbabilityOfChurn >= 0.25 && x.ProbabilityOfChurn < 0.50)
                .WhereIf(churnStatus == "MH", x => x.ProbabilityOfChurn >= 0.50 && x.ProbabilityOfChurn < 0.75)
                .WhereIf(churnStatus == "HP", x => x.ProbabilityOfChurn >= 0.75);

            var totalNoOfCustomers = await filteredChurnDto.CountAsync();

            string churnStatusDesc = "";
            switch (churnStatus)
            {
                case "LP":
                    churnStatusDesc = "Low Propensity to Churn";
                    break;
                case "MP":
                    churnStatusDesc = "Medium Low Propensity to Churn";
                    break;
                case "MH":
                    churnStatusDesc = "Medium High Propensity to Churn";
                    break;
                default:
                    churnStatusDesc = "High Propensity to Churn";
                    break;
            }

            var clusters = filteredChurnDto.GroupBy(x => x.Clusters)
                            .Select(group => new { Clusters = group.Key, Count = group.Count() })
                            .OrderByDescending(x => x.Count).Take(3)
                            .ToList();


            var CustomerList = from d in filteredChurnDto
                           select new ChurntableDTO()
                           {
                               customerID = d.CustomerID,
                               customerSegment = d.Clusters.ToString(),
                               NBO1 = d.NextBestOffer1,
                               NBO2 = d.NextBestOffer2,
                               NBO3 = d.NextBestOffer3
                           };
            var ChurnDto = new ChurnPropensityOutputDto()
            {
                churnCustomerCount = totalNoOfCustomers,
                churnStatus = churnStatusDesc,
                CustomerList = CustomerList.ToList(),
                churnSegment = clusters[0].Clusters.ToString(),
                churnSegment2 = clusters[1].Clusters.ToString(),
                churnSegment3 = clusters[2].Clusters.ToString()
            };
            return ChurnDto;
        }
        

        public async Task<PagedResultDto<GetProductRecommenderSystemNBOForViewDto>> GetAll(GetAllProductRecommenderSystemNBOInput input)
         {

            var filteredProductRecommenderSystemNBO = _productRecommenderSystemNBORepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CustomerID.Contains(input.Filter) || e.NumberOfdays.Contains(input.Filter) || e.CustomerRegistrationLocation.Contains(input.Filter) || e.LastTransactionLocation.Contains(input.Filter) || e.NextBestOffer1.Contains(input.Filter) || e.NextBestOffer2.Contains(input.Filter) || e.NextBestOffer3.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CustomerIDFilter), e => e.CustomerID.ToLower() == input.CustomerIDFilter.ToLower().Trim());
						//.WhereIf(input.MinLastTransactionDateFilter != null, e => e.LastTransactionDate >= input.MinLastTransactionDateFilter)
						//.WhereIf(input.MaxLastTransactionDateFilter != null, e => e.LastTransactionDate <= input.MaxLastTransactionDateFilter)
						//.WhereIf(!string.IsNullOrWhiteSpace(input.NumberOfdaysFilter),  e => e.NumberOfdays.ToLower() == input.NumberOfdaysFilter.ToLower().Trim())
						//.WhereIf(input.MinClustersFilter != null, e => e.Clusters >= input.MinClustersFilter)
						//.WhereIf(input.MaxClustersFilter != null, e => e.Clusters <= input.MaxClustersFilter)
						//.WhereIf(input.MinTransactionAmountFilter != null, e => e.TransactionAmount >= input.MinTransactionAmountFilter)
						//.WhereIf(input.MaxTransactionAmountFilter != null, e => e.TransactionAmount <= input.MaxTransactionAmountFilter)
						//.WhereIf(!string.IsNullOrWhiteSpace(input.CustomerRegistrationLocationFilter),  e => e.CustomerRegistrationLocation.ToLower() == input.CustomerRegistrationLocationFilter.ToLower().Trim())
						//.WhereIf(!string.IsNullOrWhiteSpace(input.LastTransactionLocationFilter),  e => e.LastTransactionLocation.ToLower() == input.LastTransactionLocationFilter.ToLower().Trim())
						//.WhereIf(input.MinProbabilityOfChurnFilter != null, e => e.ProbabilityOfChurn >= input.MinProbabilityOfChurnFilter)
						//.WhereIf(input.MaxProbabilityOfChurnFilter != null, e => e.ProbabilityOfChurn <= input.MaxProbabilityOfChurnFilter)
						//.WhereIf(input.morecliqFilter > -1,  e => Convert.ToInt32(e.morecliq) == input.morecliqFilter )
						//.WhereIf(input.moretalkFilter > -1,  e => Convert.ToInt32(e.moretalk) == input.moretalkFilter )
						//.WhereIf(input.moreflexFilter > -1,  e => Convert.ToInt32(e.moreflex) == input.moreflexFilter )
						//.WhereIf(input.cliqliteFilter > -1,  e => Convert.ToInt32(e.cliqlite) == input.cliqliteFilter )
						//.WhereIf(input.talkzoneFilter > -1,  e => Convert.ToInt32(e.talkzone) == input.talkzoneFilter )
						//.WhereIf(input.morelifeCompleteFilter > -1,  e => Convert.ToInt32(e.morelifeComplete) == input.morelifeCompleteFilter )
						//.WhereIf(input.morebusinessFilter > -1,  e => Convert.ToInt32(e.morebusiness) == input.morebusinessFilter )
						//.WhereIf(input.morelifePostpaidFilter > -1,  e => Convert.ToInt32(e.morelifePostpaid) == input.morelifePostpaidFilter )
						//.WhereIf(input.moreflexPostpaidFilter > -1,  e => Convert.ToInt32(e.moreflexPostpaid) == input.moreflexPostpaidFilter )
						//.WhereIf(input.classicPostpaidFilter > -1,  e => Convert.ToInt32(e.classicPostpaid) == input.classicPostpaidFilter )
						//.WhereIf(!string.IsNullOrWhiteSpace(input.postpaidAccount&BillPaymentFilter),  e => e.postpaidAccount&BillPayment.ToLower() == input.postpaidAccount&BillPaymentFilter.ToLower().Trim())
						//.WhereIf(input.postpaidBillBankPaymentFilter > -1,  e => Convert.ToInt32(e.postpaidBillBankPayment) == input.postpaidBillBankPaymentFilter )
						//.WhereIf(input.IDDRatesFilter > -1,  e => Convert.ToInt32(e.IDDRates) == input.IDDRatesFilter )
						//.WhereIf(input.roamingInfoFilter > -1,  e => Convert.ToInt32(e.roamingInfo) == input.roamingInfoFilter )
						//.WhereIf(input.roamingRatesFilter > -1,  e => Convert.ToInt32(e.roamingRates) == input.roamingRatesFilter )
						//.WhereIf(input.internationalGiftRechargeFilter > -1,  e => Convert.ToInt32(e.internationalGiftRecharge) == input.internationalGiftRechargeFilter )
						//.WhereIf(input.umrahHajjRoamingFilter > -1,  e => Convert.ToInt32(e.umrahHajjRoaming) == input.umrahHajjRoamingFilter )
						//.WhereIf(input.kwikcashFilter > -1,  e => Convert.ToInt32(e.kwikcash) == input.kwikcashFilter )
						//.WhereIf(input.morecreditFilter > -1,  e => Convert.ToInt32(e.morecredit) == input.morecreditFilter )
						//.WhereIf(input.automatedRechargeFilter > -1,  e => Convert.ToInt32(e.automatedRecharge) == input.automatedRechargeFilter )
						//.WhereIf(!string.IsNullOrWhiteSpace(input. more-walletFilter),  e => e. more-wallet.ToLower() == input. more-walletFilter.ToLower().Trim())
						//.WhereIf(!string.IsNullOrWhiteSpace(input. micro-insuranceFilter),  e => e. micro-insurance.ToLower() == input. micro-insuranceFilter.ToLower().Trim())
						//.WhereIf(input.moresaversFilter > -1,  e => Convert.ToInt32(e.moresavers) == input.moresaversFilter )
						//.WhereIf(input.morestatusFilter > -1,  e => Convert.ToInt32(e.morestatus) == input.morestatusFilter )
						//.WhereIf(input.moretunezFilter > -1,  e => Convert.ToInt32(e.moretunez) == input.moretunezFilter )
						//.WhereIf(input.corporateRbtFilter > -1,  e => Convert.ToInt32(e.corporateRbt) == input.corporateRbtFilter )
						//.WhereIf(input.gamesFilter > -1,  e => Convert.ToInt32(e.games) == input.gamesFilter )
						//.WhereIf(input.newsFilter > -1,  e => Convert.ToInt32(e.news) == input.newsFilter )
						//.WhereIf(input.daily25MBFilter > -1,  e => Convert.ToInt32(e.daily25MB) == input.daily25MBFilter )
						//.WhereIf(input.daily100MBFilter > -1,  e => Convert.ToInt32(e.daily100MB) == input.daily100MBFilter )
						//.WhereIf(input.weekly150MBFilter > -1,  e => Convert.ToInt32(e.weekly150MB) == input.weekly150MBFilter )
						//.WhereIf(input.Dnight1GBFilter > -1,  e => Convert.ToInt32(e.Dnight1GB) == input.Dnight1GBFilter )
						//.WhereIf(input.MNight2GBFilter > -1,  e => Convert.ToInt32(e.MNight2GB) == input.MNight2GBFilter )
						//.WhereIf(input.MNight5GBFilter > -1,  e => Convert.ToInt32(e.MNight5GB) == input.MNight5GBFilter )
						//.WhereIf(input.yearly30GBFilter > -1,  e => Convert.ToInt32(e.yearly30GB) == input.yearly30GBFilter )
						//.WhereIf(input.yearly60GBFilter > -1,  e => Convert.ToInt32(e.yearly60GB) == input.yearly60GBFilter )
						//.WhereIf(input.yearly120GBFilter > -1,  e => Convert.ToInt32(e.yearly120GB) == input.yearly120GBFilter )
						//.WhereIf(input.Monthly500MBFilter > -1,  e => Convert.ToInt32(e.Monthly500MB) == input.Monthly500MBFilter )
						//.WhereIf(input.Monthly1GBFilter > -1,  e => Convert.ToInt32(e.Monthly1GB) == input.Monthly1GBFilter )
						//.WhereIf(input.Monthly1point5GBFilter > -1,  e => Convert.ToInt32(e.Monthly1point5GB) == input.Monthly1point5GBFilter )
						//.WhereIf(input.Monthly2point5GBFilter > -1,  e => Convert.ToInt32(e.Monthly2point5GB) == input.Monthly2point5GBFilter )
						//.WhereIf(input.Monthly4GBFilter > -1,  e => Convert.ToInt32(e.Monthly4GB) == input.Monthly4GBFilter )
						//.WhereIf(input.Monthly5point5GBFilter > -1,  e => Convert.ToInt32(e.Monthly5point5GB) == input.Monthly5point5GBFilter )
						//.WhereIf(input.Monthly7point1GBFilter > -1,  e => Convert.ToInt32(e.Monthly7point1GB) == input.Monthly7point1GBFilter )
						//.WhereIf(input.Monthly11point5GBFilter > -1,  e => Convert.ToInt32(e.Monthly11point5GB) == input.Monthly11point5GBFilter )
						//.WhereIf(input.Monthly15GBFilter > -1,  e => Convert.ToInt32(e.Monthly15GB) == input.Monthly15GBFilter )
						////.WhereIf(input.Monthly27point5GBFilter > -1,  e => Convert.ToInt32(e.Monthly27point5GB) == input.Monthly27point5GBFilter )
						//.WhereIf(!string.IsNullOrWhiteSpace(input.NextBestOffer1Filter),  e => e.NextBestOffer1.ToLower() == input.NextBestOffer1Filter.ToLower().Trim())
						//.WhereIf(!string.IsNullOrWhiteSpace(input.NextBestOffer2Filter),  e => e.NextBestOffer2.ToLower() == input.NextBestOffer2Filter.ToLower().Trim())
						//.WhereIf(!string.IsNullOrWhiteSpace(input.NextBestOffer3Filter),  e => e.NextBestOffer3.ToLower() == input.NextBestOffer3Filter.ToLower().Trim());

			var pagedAndFilteredProductRecommenderSystemNBO = filteredProductRecommenderSystemNBO
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

			var productRecommenderSystemNBO = from o in pagedAndFilteredProductRecommenderSystemNBO
                         select new GetProductRecommenderSystemNBOForViewDto() {
							ProductRecommenderSystemNBO = new ProductRecommenderSystemNBODto
							{
                                CustomerID = o.CustomerID,
                                LastTransactionDate = o.LastTransactionDate,
                                NumberOfdays = o.NumberOfdays,
                                Clusters = o.Clusters,
                                TransactionAmount = o.TransactionAmount,
                                CustomerRegistrationLocation = o.CustomerRegistrationLocation,
                                LastTransactionLocation = o.LastTransactionLocation,
                                ProbabilityOfChurn = o.ProbabilityOfChurn,
                                morecliq = o.morecliq,
                                moretalk = o.moretalk,
                                moreflex = o.moreflex,
                                cliqlite = o.cliqlite,
                                talkzone = o.talkzone,
                                morelifeComplete = o.morelifeComplete,
                                morebusiness = o.morebusiness,
                                morelifePostpaid = o.morelifePostpaid,
                                moreflexPostpaid = o.moreflexPostpaid,
                                classicPostpaid = o.classicPostpaid,
                                postpaidAccountBillPayment = o.postpaidAccountBillPayment,
                                postpaidBillBankPayment = o.postpaidBillBankPayment,
                                IDDRates = o.IDDRates,
                                roamingInfo = o.roamingInfo,
                                roamingRates = o.roamingRates,
                                internationalGiftRecharge = o.internationalGiftRecharge,
                                umrahHajjRoaming = o.umrahHajjRoaming,
                                kwikcash = o.kwikcash,
                                morecredit = o.morecredit,
                                automatedRecharge = o.automatedRecharge,
                                 moreWallet = o. moreWallet,
                                 microInsurance = o. microInsurance,
                                moresavers = o.moresavers,
                                morestatus = o.morestatus,
                                moretunez = o.moretunez,
                                corporateRbt = o.corporateRbt,
                                games = o.games,
                                news = o.news,
                                daily25MB = o.daily25MB,
                                daily100MB = o.daily100MB,
                                weekly150MB = o.weekly150MB,
                                Dnight1GB = o.Dnight1GB,
                                MNight2GB = o.MNight2GB,
                                MNight5GB = o.MNight5GB,
                                yearly30GB = o.yearly30GB,
                                yearly60GB = o.yearly60GB,
                                yearly120GB = o.yearly120GB,
                                Monthly500MB = o.Monthly500MB,
                                Monthly1GB = o.Monthly1GB,
                                Monthly1point5GB = o.Monthly1point5GB,
                                Monthly2point5GB = o.Monthly2point5GB,
                                Monthly4GB = o.Monthly4GB,
                                Monthly5point5GB = o.Monthly5point5GB,
                                Monthly7point1GB = o.Monthly7point1GB,
                                Monthly11point5GB = o.Monthly11point5GB,
                                Monthly15GB = o.Monthly15GB,
                                Monthly27point5GB = o.Monthly27point5GB,
                                NextBestOffer1 = o.NextBestOffer1,
                                NextBestOffer2 = o.NextBestOffer2,
                                NextBestOffer3 = o.NextBestOffer3,
                                Id = o.Id
							}
						};

            var totalCount = await filteredProductRecommenderSystemNBO.CountAsync();

            return new PagedResultDto<GetProductRecommenderSystemNBOForViewDto>(
                totalCount,
                await productRecommenderSystemNBO.ToListAsync()
            );
         }
		 
		 public async Task<GetProductRecommenderSystemNBOForViewDto> GetProductRecommenderSystemNBOForView(int id)
         {
            var productRecommenderSystemNBO = await _productRecommenderSystemNBORepository.GetAsync(id);

            var output = new GetProductRecommenderSystemNBOForViewDto
            { ProductRecommenderSystemNBO = ObjectMapper.Map<ProductRecommenderSystemNBODto>(productRecommenderSystemNBO) };
			
            return output;
         }
		 
		 [AbpAuthorize(AppPermissions.Pages_ProductRecommenderSystemNBO_Edit)]
		 public async Task<GetProductRecommenderSystemNBOForEditOutput> GetProductRecommenderSystemNBOForEdit(EntityDto input)
         {
            var productRecommenderSystemNBO = await _productRecommenderSystemNBORepository.FirstOrDefaultAsync(input.Id);
           
		    var output = new GetProductRecommenderSystemNBOForEditOutput {ProductRecommenderSystemNBO = ObjectMapper.Map<CreateOrEditProductRecommenderSystemNBODto>(productRecommenderSystemNBO)};
			
            return output;
         }

		 public async Task CreateOrEdit(CreateOrEditProductRecommenderSystemNBODto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		 [AbpAuthorize(AppPermissions.Pages_ProductRecommenderSystemNBO_Create)]
		 protected virtual async Task Create(CreateOrEditProductRecommenderSystemNBODto input)
         {
            var productRecommenderSystemNBO = ObjectMapper.Map<ProductRecommenderSystemNBO>(input);

			
			if (AbpSession.TenantId != null)
			{
				productRecommenderSystemNBO.TenantId = (int?) AbpSession.TenantId;
			}
		

            await _productRecommenderSystemNBORepository.InsertAsync(productRecommenderSystemNBO);
         }

		 [AbpAuthorize(AppPermissions.Pages_ProductRecommenderSystemNBO_Edit)]
		 protected virtual async Task Update(CreateOrEditProductRecommenderSystemNBODto input)
         {
            var productRecommenderSystemNBO = await _productRecommenderSystemNBORepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, productRecommenderSystemNBO);
         }

		 [AbpAuthorize(AppPermissions.Pages_ProductRecommenderSystemNBO_Delete)]
         public async Task Delete(EntityDto input)
         {
            await _productRecommenderSystemNBORepository.DeleteAsync(input.Id);
         } 


		public async Task<FileDto> GetProductRecommenderSystemNBOToExcel(GetAllProductRecommenderSystemNBOForExcelInput input)
         {

            var filteredProductRecommenderSystemNBO = _productRecommenderSystemNBORepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.CustomerID.Contains(input.Filter) || e.NumberOfdays.Contains(input.Filter) || e.CustomerRegistrationLocation.Contains(input.Filter) || e.LastTransactionLocation.Contains(input.Filter) || e.NextBestOffer1.Contains(input.Filter) || e.NextBestOffer2.Contains(input.Filter) || e.NextBestOffer3.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CustomerIDFilter), e => e.CustomerID.ToLower() == input.CustomerIDFilter.ToLower().Trim());
						//.WhereIf(input.MinLastTransactionDateFilter != null, e => e.LastTransactionDate >= input.MinLastTransactionDateFilter)
						//.WhereIf(input.MaxLastTransactionDateFilter != null, e => e.LastTransactionDate <= input.MaxLastTransactionDateFilter)
						//.WhereIf(!string.IsNullOrWhiteSpace(input.NumberOfdaysFilter),  e => e.NumberOfdays.ToLower() == input.NumberOfdaysFilter.ToLower().Trim())
						//.WhereIf(input.MinClustersFilter != null, e => e.Clusters >= input.MinClustersFilter)
						//.WhereIf(input.MaxClustersFilter != null, e => e.Clusters <= input.MaxClustersFilter)
						//.WhereIf(input.MinTransactionAmountFilter != null, e => e.TransactionAmount >= input.MinTransactionAmountFilter)
						//.WhereIf(input.MaxTransactionAmountFilter != null, e => e.TransactionAmount <= input.MaxTransactionAmountFilter)
						//.WhereIf(!string.IsNullOrWhiteSpace(input.CustomerRegistrationLocationFilter),  e => e.CustomerRegistrationLocation.ToLower() == input.CustomerRegistrationLocationFilter.ToLower().Trim())
						//.WhereIf(!string.IsNullOrWhiteSpace(input.LastTransactionLocationFilter),  e => e.LastTransactionLocation.ToLower() == input.LastTransactionLocationFilter.ToLower().Trim())
						//.WhereIf(input.MinProbabilityOfChurnFilter != null, e => e.ProbabilityOfChurn >= input.MinProbabilityOfChurnFilter)
						//.WhereIf(input.MaxProbabilityOfChurnFilter != null, e => e.ProbabilityOfChurn <= input.MaxProbabilityOfChurnFilter)
						//.WhereIf(input.morecliqFilter > -1,  e => Convert.ToInt32(e.morecliq) == input.morecliqFilter )
						//.WhereIf(input.moretalkFilter > -1,  e => Convert.ToInt32(e.moretalk) == input.moretalkFilter )
						//.WhereIf(input.moreflexFilter > -1,  e => Convert.ToInt32(e.moreflex) == input.moreflexFilter )
						//.WhereIf(input.cliqliteFilter > -1,  e => Convert.ToInt32(e.cliqlite) == input.cliqliteFilter )
						//.WhereIf(input.talkzoneFilter > -1,  e => Convert.ToInt32(e.talkzone) == input.talkzoneFilter )
						//.WhereIf(input.morelifeCompleteFilter > -1,  e => Convert.ToInt32(e.morelifeComplete) == input.morelifeCompleteFilter )
						//.WhereIf(input.morebusinessFilter > -1,  e => Convert.ToInt32(e.morebusiness) == input.morebusinessFilter )
						//.WhereIf(input.morelifePostpaidFilter > -1,  e => Convert.ToInt32(e.morelifePostpaid) == input.morelifePostpaidFilter )
						//.WhereIf(input.moreflexPostpaidFilter > -1,  e => Convert.ToInt32(e.moreflexPostpaid) == input.moreflexPostpaidFilter )
						//.WhereIf(input.classicPostpaidFilter > -1,  e => Convert.ToInt32(e.classicPostpaid) == input.classicPostpaidFilter )
						//.WhereIf(!string.IsNullOrWhiteSpace(input.postpaidAccount&BillPaymentFilter),  e => e.postpaidAccount&BillPayment.ToLower() == input.postpaidAccount&BillPaymentFilter.ToLower().Trim())
						//.WhereIf(input.postpaidBillBankPaymentFilter > -1,  e => Convert.ToInt32(e.postpaidBillBankPayment) == input.postpaidBillBankPaymentFilter )
						//.WhereIf(input.IDDRatesFilter > -1,  e => Convert.ToInt32(e.IDDRates) == input.IDDRatesFilter )
						//.WhereIf(input.roamingInfoFilter > -1,  e => Convert.ToInt32(e.roamingInfo) == input.roamingInfoFilter )
						//.WhereIf(input.roamingRatesFilter > -1,  e => Convert.ToInt32(e.roamingRates) == input.roamingRatesFilter )
						//.WhereIf(input.internationalGiftRechargeFilter > -1,  e => Convert.ToInt32(e.internationalGiftRecharge) == input.internationalGiftRechargeFilter )
						//.WhereIf(input.umrahHajjRoamingFilter > -1,  e => Convert.ToInt32(e.umrahHajjRoaming) == input.umrahHajjRoamingFilter )
						//.WhereIf(input.kwikcashFilter > -1,  e => Convert.ToInt32(e.kwikcash) == input.kwikcashFilter )
						//.WhereIf(input.morecreditFilter > -1,  e => Convert.ToInt32(e.morecredit) == input.morecreditFilter )
						//.WhereIf(input.automatedRechargeFilter > -1,  e => Convert.ToInt32(e.automatedRecharge) == input.automatedRechargeFilter )
						//.WhereIf(!string.IsNullOrWhiteSpace(input. more-walletFilter),  e => e. more-wallet.ToLower() == input. more-walletFilter.ToLower().Trim())
						//.WhereIf(!string.IsNullOrWhiteSpace(input. micro-insuranceFilter),  e => e. micro-insurance.ToLower() == input. micro-insuranceFilter.ToLower().Trim())
						//.WhereIf(input.moresaversFilter > -1,  e => Convert.ToInt32(e.moresavers) == input.moresaversFilter )
						//.WhereIf(input.morestatusFilter > -1,  e => Convert.ToInt32(e.morestatus) == input.morestatusFilter )
						//.WhereIf(input.moretunezFilter > -1,  e => Convert.ToInt32(e.moretunez) == input.moretunezFilter )
						//.WhereIf(input.corporateRbtFilter > -1,  e => Convert.ToInt32(e.corporateRbt) == input.corporateRbtFilter )
						//.WhereIf(input.gamesFilter > -1,  e => Convert.ToInt32(e.games) == input.gamesFilter )
						//.WhereIf(input.newsFilter > -1,  e => Convert.ToInt32(e.news) == input.newsFilter )
						//.WhereIf(input.daily25MBFilter > -1,  e => Convert.ToInt32(e.daily25MB) == input.daily25MBFilter )
						//.WhereIf(input.daily100MBFilter > -1,  e => Convert.ToInt32(e.daily100MB) == input.daily100MBFilter )
						//.WhereIf(input.weekly150MBFilter > -1,  e => Convert.ToInt32(e.weekly150MB) == input.weekly150MBFilter )
						//.WhereIf(input.Dnight1GBFilter > -1,  e => Convert.ToInt32(e.Dnight1GB) == input.Dnight1GBFilter )
						//.WhereIf(input.MNight2GBFilter > -1,  e => Convert.ToInt32(e.MNight2GB) == input.MNight2GBFilter )
						//.WhereIf(input.MNight5GBFilter > -1,  e => Convert.ToInt32(e.MNight5GB) == input.MNight5GBFilter )
						//.WhereIf(input.yearly30GBFilter > -1,  e => Convert.ToInt32(e.yearly30GB) == input.yearly30GBFilter )
						//.WhereIf(input.yearly60GBFilter > -1,  e => Convert.ToInt32(e.yearly60GB) == input.yearly60GBFilter )
						//.WhereIf(input.yearly120GBFilter > -1,  e => Convert.ToInt32(e.yearly120GB) == input.yearly120GBFilter )
						//.WhereIf(input.Monthly500MBFilter > -1,  e => Convert.ToInt32(e.Monthly500MB) == input.Monthly500MBFilter )
						//.WhereIf(input.Monthly1GBFilter > -1,  e => Convert.ToInt32(e.Monthly1GB) == input.Monthly1GBFilter )
						//.WhereIf(input.Monthly1point5GBFilter > -1,  e => Convert.ToInt32(e.Monthly1point5GB) == input.Monthly1point5GBFilter )
						//.WhereIf(input.Monthly2point5GBFilter > -1,  e => Convert.ToInt32(e.Monthly2point5GB) == input.Monthly2point5GBFilter )
						//.WhereIf(input.Monthly4GBFilter > -1,  e => Convert.ToInt32(e.Monthly4GB) == input.Monthly4GBFilter )
						//.WhereIf(input.Monthly5point5GBFilter > -1,  e => Convert.ToInt32(e.Monthly5point5GB) == input.Monthly5point5GBFilter )
						//.WhereIf(input.Monthly7point1GBFilter > -1,  e => Convert.ToInt32(e.Monthly7point1GB) == input.Monthly7point1GBFilter )
						//.WhereIf(input.Monthly11point5GBFilter > -1,  e => Convert.ToInt32(e.Monthly11point5GB) == input.Monthly11point5GBFilter )
						//.WhereIf(input.Monthly15GBFilter > -1,  e => Convert.ToInt32(e.Monthly15GB) == input.Monthly15GBFilter )
						//.WhereIf(input.Monthly27point5GBFilter > -1,  e => Convert.ToInt32(e.Monthly27point5GB) == input.Monthly27point5GBFilter )
						//.WhereIf(!string.IsNullOrWhiteSpace(input.NextBestOffer1Filter),  e => e.NextBestOffer1.ToLower() == input.NextBestOffer1Filter.ToLower().Trim())
						//.WhereIf(!string.IsNullOrWhiteSpace(input.NextBestOffer2Filter),  e => e.NextBestOffer2.ToLower() == input.NextBestOffer2Filter.ToLower().Trim())
						//.WhereIf(!string.IsNullOrWhiteSpace(input.NextBestOffer3Filter),  e => e.NextBestOffer3.ToLower() == input.NextBestOffer3Filter.ToLower().Trim());

			var query = (from o in filteredProductRecommenderSystemNBO
                         select new GetProductRecommenderSystemNBOForViewDto() { 
							ProductRecommenderSystemNBO = new ProductRecommenderSystemNBODto
							{
                                CustomerID = o.CustomerID,
                                LastTransactionDate = o.LastTransactionDate,
                                NumberOfdays = o.NumberOfdays,
                                Clusters = o.Clusters,
                                TransactionAmount = o.TransactionAmount,
                                CustomerRegistrationLocation = o.CustomerRegistrationLocation,
                                LastTransactionLocation = o.LastTransactionLocation,
                                ProbabilityOfChurn = o.ProbabilityOfChurn,
                                morecliq = o.morecliq,
                                moretalk = o.moretalk,
                                moreflex = o.moreflex,
                                cliqlite = o.cliqlite,
                                talkzone = o.talkzone,
                                morelifeComplete = o.morelifeComplete,
                                morebusiness = o.morebusiness,
                                morelifePostpaid = o.morelifePostpaid,
                                moreflexPostpaid = o.moreflexPostpaid,
                                classicPostpaid = o.classicPostpaid,
                                postpaidAccountBillPayment = o.postpaidAccountBillPayment,
                                postpaidBillBankPayment = o.postpaidBillBankPayment,
                                IDDRates = o.IDDRates,
                                roamingInfo = o.roamingInfo,
                                roamingRates = o.roamingRates,
                                internationalGiftRecharge = o.internationalGiftRecharge,
                                umrahHajjRoaming = o.umrahHajjRoaming,
                                kwikcash = o.kwikcash,
                                morecredit = o.morecredit,
                                automatedRecharge = o.automatedRecharge,
                                 moreWallet = o. moreWallet,
                                 microInsurance = o. microInsurance,
                                moresavers = o.moresavers,
                                morestatus = o.morestatus,
                                moretunez = o.moretunez,
                                corporateRbt = o.corporateRbt,
                                games = o.games,
                                news = o.news,
                                daily25MB = o.daily25MB,
                                daily100MB = o.daily100MB,
                                weekly150MB = o.weekly150MB,
                                Dnight1GB = o.Dnight1GB,
                                MNight2GB = o.MNight2GB,
                                MNight5GB = o.MNight5GB,
                                yearly30GB = o.yearly30GB,
                                yearly60GB = o.yearly60GB,
                                yearly120GB = o.yearly120GB,
                                Monthly500MB = o.Monthly500MB,
                                Monthly1GB = o.Monthly1GB,
                                Monthly1point5GB = o.Monthly1point5GB,
                                Monthly2point5GB = o.Monthly2point5GB,
                                Monthly4GB = o.Monthly4GB,
                                Monthly5point5GB = o.Monthly5point5GB,
                                Monthly7point1GB = o.Monthly7point1GB,
                                Monthly11point5GB = o.Monthly11point5GB,
                                Monthly15GB = o.Monthly15GB,
                                Monthly27point5GB = o.Monthly27point5GB,
                                NextBestOffer1 = o.NextBestOffer1,
                                NextBestOffer2 = o.NextBestOffer2,
                                NextBestOffer3 = o.NextBestOffer3,
                                Id = o.Id
							}
						 });


            var productRecommenderSystemNBOListDtos = await query.ToListAsync();

            return _productRecommenderSystemNBOExcelExporter.ExportToFile(productRecommenderSystemNBOListDtos);
         }

        public async Task<List<GetProductRecommenderSystemNBOForViewDto>> SearchResults(GetAllProductRecommenderSystemNBOInput SearchInput)
        {

            var filteredSearchResults =  _productRecommenderSystemNBORepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(SearchInput.Filter), e => false || e.CustomerID.Contains(SearchInput.Filter) || e.NumberOfdays.Contains(SearchInput.Filter) || e.CustomerRegistrationLocation.Contains(SearchInput.Filter) || e.LastTransactionLocation.Contains(SearchInput.Filter) || e.NextBestOffer1.Contains(SearchInput.Filter) || e.NextBestOffer2.Contains(SearchInput.Filter) || e.NextBestOffer3.Contains(SearchInput.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(SearchInput.CustomerIDFilter), e => e.CustomerID.ToLower() == SearchInput.CustomerIDFilter.ToLower().Trim());

            var pagedAndFilteredProductRecommenderSystemNBO = filteredSearchResults
                .OrderBy(SearchInput.Sorting ?? "id asc")
                .PageBy(SearchInput)
                .Take(5);

            var productRecommenderSystemNBO = from o in pagedAndFilteredProductRecommenderSystemNBO
                                              select new GetProductRecommenderSystemNBOForViewDto()
                                              {
                                                  ProductRecommenderSystemNBO = new ProductRecommenderSystemNBODto
                                                  {
                                                      CustomerID = o.CustomerID,
                                                      LastTransactionDate = o.LastTransactionDate,
                                                      NumberOfdays = o.NumberOfdays,
                                                      Clusters = o.Clusters,
                                                      TransactionAmount = o.TransactionAmount,
                                                      CustomerRegistrationLocation = o.CustomerRegistrationLocation,
                                                      LastTransactionLocation = o.LastTransactionLocation,
                                                      ProbabilityOfChurn = o.ProbabilityOfChurn,
                                                      morecliq = o.morecliq,
                                                      moretalk = o.moretalk,
                                                      moreflex = o.moreflex,
                                                      cliqlite = o.cliqlite,
                                                      talkzone = o.talkzone,
                                                      morelifeComplete = o.morelifeComplete,
                                                      morebusiness = o.morebusiness,
                                                      morelifePostpaid = o.morelifePostpaid,
                                                      moreflexPostpaid = o.moreflexPostpaid,
                                                      classicPostpaid = o.classicPostpaid,
                                                      postpaidAccountBillPayment = o.postpaidAccountBillPayment,
                                                      postpaidBillBankPayment = o.postpaidBillBankPayment,
                                                      IDDRates = o.IDDRates,
                                                      roamingInfo = o.roamingInfo,
                                                      roamingRates = o.roamingRates,
                                                      internationalGiftRecharge = o.internationalGiftRecharge,
                                                      umrahHajjRoaming = o.umrahHajjRoaming,
                                                      kwikcash = o.kwikcash,
                                                      morecredit = o.morecredit,
                                                      automatedRecharge = o.automatedRecharge,
                                                      moreWallet = o.moreWallet,
                                                      microInsurance = o.microInsurance,
                                                      moresavers = o.moresavers,
                                                      morestatus = o.morestatus,
                                                      moretunez = o.moretunez,
                                                      corporateRbt = o.corporateRbt,
                                                      games = o.games,
                                                      news = o.news,
                                                      daily25MB = o.daily25MB,
                                                      daily100MB = o.daily100MB,
                                                      weekly150MB = o.weekly150MB,
                                                      Dnight1GB = o.Dnight1GB,
                                                      MNight2GB = o.MNight2GB,
                                                      MNight5GB = o.MNight5GB,
                                                      yearly30GB = o.yearly30GB,
                                                      yearly60GB = o.yearly60GB,
                                                      yearly120GB = o.yearly120GB,
                                                      Monthly500MB = o.Monthly500MB,
                                                      Monthly1GB = o.Monthly1GB,
                                                      Monthly1point5GB = o.Monthly1point5GB,
                                                      Monthly2point5GB = o.Monthly2point5GB,
                                                      Monthly4GB = o.Monthly4GB,
                                                      Monthly5point5GB = o.Monthly5point5GB,
                                                      Monthly7point1GB = o.Monthly7point1GB,
                                                      Monthly11point5GB = o.Monthly11point5GB,
                                                      Monthly15GB = o.Monthly15GB,
                                                      Monthly27point5GB = o.Monthly27point5GB,
                                                      NextBestOffer1 = o.NextBestOffer1,
                                                      NextBestOffer2 = o.NextBestOffer2,
                                                      NextBestOffer3 = o.NextBestOffer3,
                                                      Id = o.Id
                                                  }
                                              };

            var totalCount = await filteredSearchResults.CountAsync();

            return await productRecommenderSystemNBO.ToListAsync();
        }


    }

}



