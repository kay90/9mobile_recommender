using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using Abp.Auditing;

namespace TestDemo.ProductRecommenderSystem
{
	[Table("ProductRecommenderSystemNBO")]
    [Audited]
    public class ProductRecommenderSystemNBO : FullAuditedEntity , IMayHaveTenant
    {
			public int? TenantId { get; set; }
			

		public virtual string CustomerID { get; set; }
		
		public virtual DateTime? LastTransactionDate { get; set; }
		
		public virtual string NumberOfdays { get; set; }
		
		public virtual int? Clusters { get; set; }
		
		public virtual double? TransactionAmount { get; set; }
		
		public virtual string CustomerRegistrationLocation { get; set; }
		
		public virtual string LastTransactionLocation { get; set; }
		
		public virtual double? ProbabilityOfChurn { get; set; }
		
		public virtual bool morecliq { get; set; }
		
		public virtual bool moretalk { get; set; }
		
		public virtual bool moreflex { get; set; }
		
		public virtual bool cliqlite { get; set; }
		
		public virtual bool talkzone { get; set; }
		
		public virtual bool morelifeComplete { get; set; }
		
		public virtual bool morebusiness { get; set; }
		
		public virtual bool morelifePostpaid { get; set; }
		
		public virtual bool moreflexPostpaid { get; set; }
		
		public virtual bool classicPostpaid { get; set; }
		
		public virtual bool postpaidAccountBillPayment { get; set; }
		
		public virtual bool postpaidBillBankPayment { get; set; }
		
		public virtual bool IDDRates { get; set; }
		
		public virtual bool roamingInfo { get; set; }
		
		public virtual bool roamingRates { get; set; }
		
		public virtual bool internationalGiftRecharge { get; set; }
		
		public virtual bool umrahHajjRoaming { get; set; }
		
		public virtual bool kwikcash { get; set; }
		
		public virtual bool morecredit { get; set; }
		
		public virtual bool automatedRecharge { get; set; }
		
		public virtual bool  moreWallet { get; set; }
		
		public virtual bool  microInsurance { get; set; }
		
		public virtual bool moresavers { get; set; }
		
		public virtual bool morestatus { get; set; }
		
		public virtual bool moretunez { get; set; }
		
		public virtual bool corporateRbt { get; set; }
		
		public virtual bool games { get; set; }
		
		public virtual bool news { get; set; }
		
		public virtual bool daily25MB { get; set; }
		
		public virtual bool daily100MB { get; set; }
		
		public virtual bool weekly150MB { get; set; }
		
		public virtual bool Dnight1GB { get; set; }
		
		public virtual bool MNight2GB { get; set; }
		
		public virtual bool MNight5GB { get; set; }
		
		public virtual bool yearly30GB { get; set; }
		
		public virtual bool yearly60GB { get; set; }
		
		public virtual bool yearly120GB { get; set; }
		
		public virtual bool Monthly500MB { get; set; }
		
		public virtual bool Monthly1GB { get; set; }
		
		public virtual bool Monthly1point5GB { get; set; }
		
		public virtual bool Monthly2point5GB { get; set; }
		
		public virtual bool Monthly4GB { get; set; }
		
		public virtual bool Monthly5point5GB { get; set; }
		
		public virtual bool Monthly7point1GB { get; set; }
		
		public virtual bool Monthly11point5GB { get; set; }
		
		public virtual bool Monthly15GB { get; set; }
		
		public virtual bool Monthly27point5GB { get; set; }
		
		public virtual string NextBestOffer1 { get; set; }
		
		public virtual string NextBestOffer2 { get; set; }
		
		public virtual string NextBestOffer3 { get; set; }
		

    }
}