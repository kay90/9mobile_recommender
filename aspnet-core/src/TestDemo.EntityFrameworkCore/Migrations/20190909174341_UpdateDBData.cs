﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TestDemo.Migrations
{
    public partial class UpdateDBData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProductRecommenderSystemNBO",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    TenantId = table.Column<int>(nullable: true),
                    CustomerID = table.Column<string>(nullable: true),
                    LastTransactionDate = table.Column<DateTime>(nullable: true),
                    NumberOfdays = table.Column<string>(nullable: true),
                    Clusters = table.Column<int>(nullable: true),
                    TransactionAmount = table.Column<double>(nullable: true),
                    CustomerRegistrationLocation = table.Column<string>(nullable: true),
                    LastTransactionLocation = table.Column<string>(nullable: true),
                    ProbabilityOfChurn = table.Column<double>(nullable: true),
                    morecliq = table.Column<bool>(nullable: false),
                    moretalk = table.Column<bool>(nullable: false),
                    moreflex = table.Column<bool>(nullable: false),
                    cliqlite = table.Column<bool>(nullable: false),
                    talkzone = table.Column<bool>(nullable: false),
                    morelifeComplete = table.Column<bool>(nullable: false),
                    morebusiness = table.Column<bool>(nullable: false),
                    morelifePostpaid = table.Column<bool>(nullable: false),
                    moreflexPostpaid = table.Column<bool>(nullable: false),
                    classicPostpaid = table.Column<bool>(nullable: false),
                    postpaidAccountBillPayment = table.Column<bool>(nullable: false),
                    postpaidBillBankPayment = table.Column<bool>(nullable: false),
                    IDDRates = table.Column<bool>(nullable: false),
                    roamingInfo = table.Column<bool>(nullable: false),
                    roamingRates = table.Column<bool>(nullable: false),
                    internationalGiftRecharge = table.Column<bool>(nullable: false),
                    umrahHajjRoaming = table.Column<bool>(nullable: false),
                    kwikcash = table.Column<bool>(nullable: false),
                    morecredit = table.Column<bool>(nullable: false),
                    automatedRecharge = table.Column<bool>(nullable: false),
                    moreWallet = table.Column<bool>(nullable: false),
                    microInsurance = table.Column<bool>(nullable: false),
                    moresavers = table.Column<bool>(nullable: false),
                    morestatus = table.Column<bool>(nullable: false),
                    moretunez = table.Column<bool>(nullable: false),
                    corporateRbt = table.Column<bool>(nullable: false),
                    games = table.Column<bool>(nullable: false),
                    news = table.Column<bool>(nullable: false),
                    daily25MB = table.Column<bool>(nullable: false),
                    daily100MB = table.Column<bool>(nullable: false),
                    weekly150MB = table.Column<bool>(nullable: false),
                    Dnight1GB = table.Column<bool>(nullable: false),
                    MNight2GB = table.Column<bool>(nullable: false),
                    MNight5GB = table.Column<bool>(nullable: false),
                    yearly30GB = table.Column<bool>(nullable: false),
                    yearly60GB = table.Column<bool>(nullable: false),
                    yearly120GB = table.Column<bool>(nullable: false),
                    Monthly500MB = table.Column<bool>(nullable: false),
                    Monthly1GB = table.Column<bool>(nullable: false),
                    Monthly1point5GB = table.Column<bool>(nullable: false),
                    Monthly2point5GB = table.Column<bool>(nullable: false),
                    Monthly4GB = table.Column<bool>(nullable: false),
                    Monthly5point5GB = table.Column<bool>(nullable: false),
                    Monthly7point1GB = table.Column<bool>(nullable: false),
                    Monthly11point5GB = table.Column<bool>(nullable: false),
                    Monthly15GB = table.Column<bool>(nullable: false),
                    Monthly27point5GB = table.Column<bool>(nullable: false),
                    NextBestOffer1 = table.Column<string>(nullable: true),
                    NextBestOffer2 = table.Column<string>(nullable: true),
                    NextBestOffer3 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductRecommenderSystemNBO", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductRecommenderSystemNBO_TenantId",
                table: "ProductRecommenderSystemNBO",
                column: "TenantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductRecommenderSystemNBO");
        }
    }
}
